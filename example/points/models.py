# coding: utf-8
from django.contrib.gis.db import models


class Point900913(models.Model):
    """Simple class for testing with 900913 srid"""

    name = models.CharField(max_length=10)

    geom = models.PointField(srid=900913)

    def __unicode__(self):

        return self.name


class Point(models.Model):

    """Simple class for testing"""

    name = models.CharField(max_length=10)

    geom = models.PointField()

    def __unicode__(self):

        return self.name
