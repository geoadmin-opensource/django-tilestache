============
Installation
============

At the command line::

    $ easy_install django-tilestache

Or, if you have virtualenvwrapper installed::

    $ mkvirtualenv django-tilestache
    $ pip install django-tilestache
