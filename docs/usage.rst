=====
Usage
=====

To use Django TileStache in a project, add it to your `INSTALLED_APPS`:

.. code-block:: python

    INSTALLED_APPS = (
        ...
        'django_tilestache.apps.DjangoTilestacheConfig',
        ...
    )

Add Django TileStache's URL patterns:

.. code-block:: python

    from django_tilestache import urls as django_tilestache_urls


    urlpatterns = [
        ...
        url(r'^', include(django_tilestache_urls)),
        ...
    ]
