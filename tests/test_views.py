# coding: utf-8
from django.conf import settings
from django.core.urlresolvers import reverse
from rest_framework import status
from rest_framework.test import APITransactionTestCase
from django_tilestache.models import Layer


class TileStacheTileTestCase(APITransactionTestCase):

    """tests for tilestache tile view"""

    def test_get_tile_without_layer(self):
        url = reverse(
            'django_tilestache:tilestache-tile',
            kwargs={
                'layer_name': 'foo',
                'z': 0,
                'x': 0,
                'y': 0,
                'extension': 'pbf'
            }
        )
        self.assertEqual(
            url,
            '/api/tilestache/foo/0/0/0.pbf'
        )
        response = self.client.get(url)
        self.assertEqual(Layer.objects.all().count(), 0)
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

    def test_get_tile_with_layer(self):
        Layer.objects.create(**{
            'name': 'foo',
            'provider': {'name': 'proxy', 'url': 'http://tile.openstreetmap.org/{Z}/{X}/{Y}.png'},
        })
        url = reverse(
            'django_tilestache:tilestache-tile',
            kwargs={
                'layer_name': 'foo',
                'z': 0,
                'x': 0,
                'y': 0,
                'extension': 'png'
            }
        )
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_get_tile_vector(self):

        Layer.objects.create(**{
            'name': 'foo',
            'provider': {
                'name': 'vector',
                'driver': 'spatialite',
                'parameters': {
                    'file': settings.DATABASES['default']['NAME'],
                    'layer': 'points_point'
                }
            }
        })
        url = reverse(
            'django_tilestache:tilestache-tile',
            kwargs={
                'layer_name': 'foo',
                'z': 0,
                'x': 0,
                'y': 0,
                'extension': 'geojson'
            }
        )
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)


class TileStacheConfigurationTestCase(APITransactionTestCase):

    """
    contains the tests for
    tilestache configuration
    """

    def test_get_configuration(self):
        url = reverse('django_tilestache:tilestache-configuration')
        response = self.client.get(url, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertIn('layers', response.data)
        self.assertIn('cache', response.data)
        self.assertIn('dirpath', response.data)
