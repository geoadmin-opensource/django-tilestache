# coding: utf-8
from django.test import SimpleTestCase, TestCase
from django_tilestache.compilers import (
    TileStacheQueryCompiler,
    VecTilesQueryCompiler,
)
from example.points.models import (
    Point,
    Point900913,
)


class TileStacheQueryCompilerTestCase(SimpleTestCase):

    def test_init(self):

        tsqc = TileStacheQueryCompiler()
        self.assertIsNotNone(tsqc)


class VecTilesQueryCompilerTestCase(TestCase):

    def test_init(self):
        query_compiler = VecTilesQueryCompiler()
        self.assertIsNotNone(query_compiler)

    def test_compile_id(self):
        query_compiler = VecTilesQueryCompiler()
        result = query_compiler.compile_id()
        self.assertEqual(
            result,
            'id as __id__'
        )

    def test_compile_custom_id(self):

        query_compiler = VecTilesQueryCompiler()
        result = query_compiler.compile_id('foo')
        self.assertEqual(
            result,
            'foo as __id__'
        )

    def test_compile_geometry(self):

        query_compiler = VecTilesQueryCompiler()
        result = query_compiler.compile_geometry(
            Point.objects.all(),
            'geom'
        )
        self.assertEqual(
            result,
            'ST_Transform(geom, 900913) as __geometry__'
        )

    def test_compile_geometry_900913(self):

        query_compiler = VecTilesQueryCompiler()
        result = query_compiler.compile_geometry(
            Point900913.objects.all(),
            'geom'
        )
        self.assertEqual(
            result,
            'geom as __geometry__'
        )

    def test_select_attributes_all(self):

        query_compiler = VecTilesQueryCompiler()
        attrs = query_compiler.select_attributes(
            Point.objects.all(),
            '__all__'
        )

        self.assertEqual(len(attrs), 1)
        self.assertEqual(attrs[0], 'name')

    def test_select_attributes_none(self):

        query_compiler = VecTilesQueryCompiler()
        attrs = query_compiler.select_attributes(
            Point.objects.all(),
            list()
        )

        self.assertEqual(len(attrs), 0)

    def test_select_attributes_just_a_few(self):
        query_compiler = VecTilesQueryCompiler()
        attrs = query_compiler.select_attributes(
            Point.objects.all(),
            ['name', ]
        )
        self.assertEqual(len(attrs), 1)
        self.assertEqual(attrs[0], 'name')

    def test_compile(self):

        query_compiler = VecTilesQueryCompiler()
        result = query_compiler.compile(
            Point.objects.all(),
            '__all__'
        )
        self.assertEqual(
            result,
            '''SELECT id as __id__, ST_Transform(geom, 900913) as __geometry__, "points_point"."name" FROM "points_point"'''  # noqa
        )
