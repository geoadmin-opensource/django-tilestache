# coding: utf-8
import os
import json
import codecs
from django.conf import settings
from django.test import TransactionTestCase
from django_tilestache.models import Layer
from django_tilestache.configuration import (
    get_config,
    ConfigurationManager
)
from django_tilestache.caches import TestCacheConfig


class GetConfigTestCase(TransactionTestCase):

    def test_get_config_cache(self):

        layer = Layer(**{
            'name': 'foo',
            'provider': {'class': 'fooklass'}
        })
        layer.save()
        conf = get_config()
        self.assertIsNotNone(conf)


class ConfigurationManagerTestCase(TransactionTestCase):

    TEMP_PATH = os.path.join(settings.BASE_DIR, 'tilestache.cfg')

    def remove_temp_file(self):
        if os.path.isfile(self.TEMP_PATH):
            os.remove(self.TEMP_PATH)

    def tearDown(self):
        self.remove_temp_file()

    def test_init(self):

        config = ConfigurationManager()
        self.assertIsNotNone(config)

    def test_write_without_path(self):

        config = ConfigurationManager()
        with self.assertRaises(ValueError):
            config.write(None, object(), list())

    def test_write_without_configs(self):

        config = ConfigurationManager()
        config.write(self.TEMP_PATH, None, None)
        self.assertTrue(os.path.exists(self.TEMP_PATH))
        with codecs.open(self.TEMP_PATH, 'r', encoding='utf-8') as read:
            data = read.read()
        dic = json.loads(data)
        self.assertDictEqual(
            {
                'dirpath': os.path.dirname(self.TEMP_PATH),
                'cache': {'name': 'Test'},
                'layers': {}
            },
            dic
        )

    def test_write_with_configs(self):

        conf_man = ConfigurationManager()
        test_cache = TestCacheConfig()
        conf_man.write(self.TEMP_PATH,
                       cache=test_cache,
                       layers=dict())

        with codecs.open(self.TEMP_PATH, 'r', encoding='utf-8') as read:
            data = read.read()

        dic = json.loads(data)
        self.assertDictEqual(
            {
                'dirpath': os.path.dirname(self.TEMP_PATH),
                'cache': {
                    'name': 'Test',
                    'verbose': True
                },
                'layers': {}
            },
            dic
        )

    def test_write_with_layers(self):

        conf_man = ConfigurationManager()
        test_cache = TestCacheConfig()
        layer_a = Layer.objects.create(**{
            'name': 'foo',
            'provider': {'class': 'fooklass'}
        })
        layer_b = Layer.objects.create(**{
            'name': 'bar',
            'allowed_origin': '*',
            'provider': {'class': 'fooklass', 'kwargs': {'one': 1, 'two': 2}}
        })

        self.maxDiff = None

        layers = [layer_a, layer_b]

        conf_man.write(self.TEMP_PATH,
                       cache=test_cache,
                       layers=layers)

        with codecs.open(self.TEMP_PATH, 'r', encoding='utf-8') as read:
            data = read.read()

        dic = json.loads(data)
        self.assertEqual(
            {
                u'dirpath': unicode(os.path.dirname(self.TEMP_PATH)),
                u'cache': {
                    u'name': u'Test',
                    u'verbose': True
                },
                u'layers': {
                    u'foo': {
                        u'provider': {
                            u'class': u'fooklass'
                        }
                    },
                    u'bar': {
                        u'provider': {
                            u'class': u'fooklass',
                            u'kwargs': {
                                u'one': 1,
                                u'two': 2
                            }
                        },
                        u'allowed origin': u'*'
                    }
                }
            },
            dic
        )
