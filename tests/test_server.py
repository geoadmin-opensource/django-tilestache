# coding: utf-8
from mock import patch
from TileStache import parseConfig
from TileStache.Caches import Test
from django.urls import reverse
from rest_framework.test import APITransactionTestCase
from django_tilestache.server import RemoteConfigurationLoader
from django_tilestache.models import Layer


class RemoteConfigurationTestCase(APITransactionTestCase):

    def test_init_no_crash(self):

        config = RemoteConfigurationLoader(endpoint='http://foo', crash_tilestache=False)
        # this request will fail, therefore this should be set to
        # the default (empty dict)
        self.assertIsNotNone(config)
        self.assertEqual(config.cache, {})
        self.assertEqual(config.layers, {})
        self.assertIsNone(config.dirpath, None)

    def test_init_crash(self):
        with self.assertRaises(Exception):
            configurator = RemoteConfigurationLoader(endpoint='http://foo', crash_tilestache=True)
            configurator.load_remote_config()

    def test_with_django_endpoint(self):
        url = 'http://localhost:8000/api/tilestache/'
        config = RemoteConfigurationLoader(endpoint=url,
                                           crash_tilestache=False)
        self.assertIsNotNone(config)
        self.assertEqual(config.cache, {})
        self.assertEqual(config.layers, {})
        self.assertIsNone(config.dirpath, None)

    def test_with_django_endpoint_and_layer(self):
        layer = Layer(**{
            'name': 'foo',
            'provider': {'name': 'proxy', 'url': 'http://foo.com.br/layers/'}
        })
        layer.save()
        self.assertEqual(Layer.objects.all().count(), 1)
        part_url = reverse('django_tilestache:tilestache-configuration')
        full_url = 'http://localhost:8000{0}'.format(part_url)
        response = self.client.get(part_url, format='json')
        config = parseConfig(response.json())
        with patch.object(RemoteConfigurationLoader, 'load_remote_config', return_value=config):
            configurator = RemoteConfigurationLoader(endpoint=full_url,
                                                     crash_tilestache=False)
            config = configurator.load_remote_config()
            self.assertIsNotNone(config)
            self.assertTrue(isinstance(config.cache, Test))
            self.assertEqual(len(config.layers.keys()), 1)
            self.assertIn('foo', config.layers)
            self.assertEqual(config.dirpath, '.')
