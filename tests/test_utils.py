# coding: utf-8
from mock import MagicMock, PropertyMock
from django.contrib.gis.geos import Point
from django.test import SimpleTestCase
from ModestMaps.Geo import Location
from django_tilestache.utils import (
    geometry_to_bounds,
)


class GeometryToBoundsTestCase(SimpleTestCase):

    def test_with_null_geometry(self):

        """
        tests that null geometries
        will generate an error
        """

        with self.assertRaises(ValueError):
            geometry_to_bounds(None)

    def test_empty_geometry(self):

        """
        tests that empty geometries will throw an error
        """
        with self.assertRaises(ValueError):
            geometry = MagicMock()
            type(geometry).empty = PropertyMock(return_value=True)
            geometry_to_bounds(geometry)

    def test_impossible_null_envelope(self):

        """
        tests that it will throw an error
        if the envelope is null
        """

        with self.assertRaises(ValueError):
            geometry = MagicMock()
            type(geometry).envelope = PropertyMock(return_value=None)
            geometry_to_bounds(geometry)

    def test_basic_circle(self):

        """
        tests that the convertion from a django geometry
        results in correct tilestache bounds
        """

        geometry = Point(x=0, y=0, srid=4326).buffer(1)
        expected = {
            'nw': Location(1.0, -1.0),
            'se': Location(-1.0, 1.0)
        }

        bounds = geometry_to_bounds(geometry)
        self.assertEqual(bounds['nw'].lat, expected['nw'].lat)
        self.assertEqual(bounds['nw'].lon, expected['nw'].lon)
        self.assertEqual(bounds['se'].lat, expected['se'].lat)
        self.assertEqual(bounds['se'].lon, expected['se'].lon)
