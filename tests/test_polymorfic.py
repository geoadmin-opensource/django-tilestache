# coding: utf-8
from django.test import SimpleTestCase
from rest_framework import serializers
from rest_framework.serializers import Serializer
from django_tilestache.polymorfic import (PolySerializer,
                                          PolymorficSerializer,)


class MockOne(Serializer):

    name = serializers.CharField(
        max_length=100
    )


class MockTwo(Serializer):

    number = serializers.IntegerField(
        min_value=1,
        max_value=10
    )


class BadPoly(PolymorficSerializer):

    poly_serializers = None


class MockPoly(PolymorficSerializer):

    poly_serializers = (
        PolySerializer(
            MockOne,
            lambda data, context: 'name' in data,
            lambda data, context: 'name' in data
        ),
        PolySerializer(
            MockTwo,
            lambda data, context: 'number' in data,
            lambda data, context: 'number' in data
        )
    )


class PolymorficSerializerTestCase(SimpleTestCase):

    def test_init(self):

        poly = MockPoly()
        self.assertIsNotNone(poly)

    def test_validate_good_poly(self):

        """
        Makes sure that no errors will
        be thrown if this is a good
        poly_serializer (poly_serializers != None)
        """

        poly = MockPoly()
        self.assertIsNotNone(poly)
        self.assertTrue(poly.validate_poly_serializer())

    def test_validate_bad_poly(self):

        """
        Test to make sure that errors will be thrown
        if the user does not define poly_serializers
        """

        poly = BadPoly(data={'foo': 'bar'})
        with self.assertRaises(ValueError):
            poly.validate_poly_serializer()

    def test_is_valid_one(self):

        poly = MockPoly(data={'name': 'foo'})
        self.assertTrue(poly.is_valid())

    def test_is_valid_two(self):
        poly = MockPoly(data={'number': 1})
        self.assertTrue(poly.is_valid())

    def test_is_not_valid(self):
        poly = MockPoly(data={'date': 'foo'})
        with self.assertRaises(ValueError):
            poly.is_valid()

    def test_from_data_to_object_one(self):
        data = {'name': 'foo'}
        poly = MockPoly(data=data)
        self.assertTrue(poly.is_valid())
        instance = poly.to_internal_value(data)
        self.assertDictEqual(instance, data)

    def test_from_data_to_object_two(self):
        data = {'number': 1}
        poly = MockPoly(data=data)
        self.assertTrue(poly.is_valid())
        instance = poly.to_internal_value(data)
        self.assertDictEqual(instance, data)

    def test_from_object_to_data_one(self):
        instance = {'name': 'bar'}
        poly = MockPoly(instance=instance)
        representation = poly.to_representation(instance)
        self.assertDictEqual(representation, instance)

    def test_from_object_to_data_two(self):
        instance = {'number': 1}
        poly = MockPoly(instance=instance)
        representation = poly.to_representation(instance)
        self.assertDictEqual(representation, instance)


class PolySerializerTestCase(SimpleTestCase):

    def test_init(self):

        poly = PolySerializer(
            Serializer(),
            lambda data, context: True,
            lambda data, context: True
        )
        self.assertIsNotNone(poly)

    def test_init_fails_without_serializer(self):

        with self.assertRaises(ValueError):
            PolySerializer(
                None,
                lambda x: True,
                lambda x: True
            )

    def test_init_fails_without_can_serialize(self):

        with self.assertRaises(ValueError):
            PolySerializer(
                Serializer(),
                None,
                lambda x: True
            )

    def test_init_fails_without_can_deserialize(self):

        with self.assertRaises(ValueError):
            PolySerializer(
                Serializer(),
                lambda x: True,
                None
            )

    def test_can_serialize(self):

        poly = PolySerializer(
            Serializer(),
            lambda data, context: data is not None and context is not None,
            lambda data, context: data is not None and context is not None
        )
        self.assertTrue(poly.can_serialize(object(), object()))

    def test_can_deserialize(self):

        poly = PolySerializer(
            Serializer(),
            lambda data, context: data is not None and context is not None,
            lambda data, context: data is not None and context is not None
        )
        self.assertTrue(poly.can_serialize(object(), object()))
