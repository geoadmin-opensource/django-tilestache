# coding: utf-8
from rest_framework.serializers import ValidationError
from django.test import SimpleTestCase, TestCase, TransactionTestCase
from django_tilestache.models import Layer
from django_tilestache.choices import (VECTOR_DRIVER_POSTGRES,
                                       VECTOR_DRIVER_SPATIALITE,)
from django_tilestache.serializers import (BoundsSerializer,
                                           LayerSerializer,
                                           VectorProviderSerializer,
                                           ProxyProviderSerializer,
                                           ExternalProviderSerializer,
                                           MapnikProviderSerializer,)


class LayerSerializerValidTestCase(TestCase):

    def test_init(self):

        serializer = LayerSerializer()
        self.assertIsNotNone(serializer)

    def test_is_valid_external(self):
        data = {
            'name': 'layer',
            'provider': {
                'class': 'foo',
                'kwargs': {'foo': 1, 'bar': 2}
            }
        }
        serializer = LayerSerializer(data=data)
        self.assertTrue(serializer.is_valid())

    def test_is_valid_proxy(self):
        data = {
            'name': 'layer',
            'provider': {
                'name': 'proxy',
                'url': 'foo',
            }
        }
        serializer = LayerSerializer(data=data)
        self.assertTrue(serializer.is_valid())

    def test_is_valid_mapnik(self):
        data = {
            'name': 'layer',
            'provider': {
                'name': 'mapnik',
                'mapfile': 'foo',
            }
        }
        serializer = LayerSerializer(data=data)
        self.assertTrue(serializer.is_valid())

    def test_is_valid_vector(self):
        data = {
            'name': 'layer',
            'provider': {
                'name': 'vector',
                'driver': VECTOR_DRIVER_POSTGRES,
                'parameters': {
                    'dbname': 'foo',
                    'host': 'bar'
                }
            }
        }
        serializer = LayerSerializer(data=data)
        self.assertTrue(serializer.is_valid())


class LayerSerializerTestCase(TransactionTestCase):

    def test_serialize_vector(self):

        layer = Layer.objects.create(
            name='foo',
            provider={
                'name': 'vector',
                'driver': VECTOR_DRIVER_POSTGRES,
                'parameters': {
                    'dbname': 'foo',
                    'host': 'bar'
                }
            }
        )
        serializer = LayerSerializer(instance=layer)
        data = serializer.data
        self.assertDictEqual(
            data,
            {
                'id': layer.id,
                'name': 'foo',
                'provider': {
                    'name': 'vector',
                    'driver': VECTOR_DRIVER_POSTGRES,
                    'parameters': {
                        'dbname': 'foo',
                        'host': 'bar'
                    }
                }
            }
        )

    def test_serialize_vector_with_allowed_origin(self):

        layer = Layer.objects.create(
            name='foo',
            provider={
                'name': 'vector',
                'driver': VECTOR_DRIVER_POSTGRES,
                'parameters': {
                    'dbname': 'foo',
                    'host': 'bar'
                }
            },
            allowed_origin='*'
        )
        self.assertEqual(layer.allowed_origin, '*')
        serializer = LayerSerializer(instance=layer)
        data = serializer.data
        self.assertDictEqual(
            data,
            {
                u'id': layer.id,
                'name': u'foo',
                'provider': {
                    'name': u'vector',
                    'driver': unicode(VECTOR_DRIVER_POSTGRES),
                    'parameters': {
                        'host': 'bar',
                        'dbname': 'foo'
                    }
                },
                'allowed origin': '*'
            }
        )

    def test_serialize_vector_extra_data(self):

        layer = Layer.objects.create(
            name='foo',
            provider={
                'name': 'vector',
                'driver': VECTOR_DRIVER_POSTGRES,
                'parameters': {
                    'dbname': 'foo',
                    'host': 'bar'
                }
            },
            metatile={
                'rows': 1,
                'columns': 1,
                'buffer': 1
            }
        )
        serializer = LayerSerializer(instance=layer)
        data = serializer.data
        self.assertDictEqual(
            data,
            {
                'id': layer.id,
                'name': 'foo',
                'provider': {
                    'name': 'vector',
                    'driver': VECTOR_DRIVER_POSTGRES,
                    'parameters': {
                        'dbname': 'foo',
                        'host': 'bar'
                    }
                },
                'metatile': {
                    'rows': 1,
                    'columns': 1,
                    'buffer': 1
                }
            }
        )

    def test_deserialize_vector(self):

        data = {
            'name': 'foo',
            'provider': {
                'name': 'vector',
                'driver': VECTOR_DRIVER_POSTGRES,
                'parameters': {
                    'dbname': 'foo',
                    'host': 'bar'
                }
            }
        }
        serializer = LayerSerializer(data=data)
        self.assertTrue(serializer.is_valid())
        layer = serializer.save()
        self.assertIsNotNone(layer)
        self.assertIsNotNone(layer.provider)
        self.assertIsNone(layer.metatile)
        self.assertEqual(dict(layer.provider), data['provider'])

    def test_update_vector(self):

        data = {
            'name': 'foo',
            'provider': {
                'name': 'vector',
                'driver': VECTOR_DRIVER_POSTGRES,
                'parameters': {
                    'dbname': 'foo',
                    'host': 'bar'
                }
            }
        }
        serializer = LayerSerializer(data=data)
        self.assertTrue(serializer.is_valid())

        layer = serializer.save()
        self.assertEqual(layer.name, 'foo')

        data = {
            'name': 'bar',
            'provider': {
                'name': 'vector',
                'driver': VECTOR_DRIVER_SPATIALITE,
                'parameters': {
                    'host': 'foo',
                    'dbname': 'bar'
                }
            }
        }

        new_layer = serializer.update(instance=layer, validated_data=data)
        self.assertEqual(layer.id, new_layer.id)
        self.assertEqual(new_layer.name, 'bar')
        self.assertEqual(
            new_layer.provider,
            {
                'name': 'vector',
                'driver': VECTOR_DRIVER_SPATIALITE,
                'parameters': {
                    'host': 'foo',
                    'dbname': 'bar'
                }
            }
        )

    def test_partial_update(self):

        data = {
            'name': 'foo',
            'provider': {
                'name': 'vector',
                'driver': VECTOR_DRIVER_POSTGRES,
                'parameters': {
                    'dbname': 'foo',
                    'host': 'bar'
                }
            }
        }
        serializer = LayerSerializer(data=data)
        self.assertTrue(serializer.is_valid())

        layer = serializer.save()
        self.assertEqual(layer.name, 'foo')

        data = {
            'name': 'new name'
        }

        new_layer = serializer.update(layer, data)
        self.assertEqual(layer.id, new_layer.id)
        self.assertEqual(new_layer.name, 'new name')

    def test_partial_update_method2(self):

        """
        makes sure that you do partial updates in this serializer
        """

        layer = Layer.objects.create(
            name='foo',
            provider={
                'name': 'vector',
                'driver': VECTOR_DRIVER_POSTGRES,
                'parameters': {
                    'dbname': 'foo',
                    'host': 'bar'
                }
            },
            metatile={
                'rows': 1,
                'columns': 1,
                'buffer': 1
            }
        )
        serializer = LayerSerializer(instance=layer, data={'name': 'new name'}, partial=True)
        self.assertTrue(serializer.is_valid())
        new_layer = serializer.save()
        self.assertEqual(new_layer.name, 'new name')

    def test_update_without_partial_fails(self):
        """
        This test makes sure that if
        you dont do partial updates, it will
        fail accordingly
        """
        layer = Layer.objects.create(
            name='foo',
            provider={
                'name': 'vector',
                'driver': VECTOR_DRIVER_POSTGRES,
                'parameters': {
                    'dbname': 'foo',
                    'host': 'bar'
                }
            },
            metatile={
                'rows': 1,
                'columns': 1,
                'buffer': 1
            }
        )
        serializer = LayerSerializer(instance=layer, data={'name': 'new name'})
        self.assertFalse(serializer.is_valid())

    def test_update_invalid_vector(self):
        data = {
            'name': 'foo',
            'provider': {
                'name': 'vector',
                'driver': VECTOR_DRIVER_POSTGRES,
                'parameters': {
                    'dbname': 'foo',
                    'host': 'bar'
                }
            }
        }
        serializer = LayerSerializer(data=data)
        self.assertTrue(serializer.is_valid())

        layer = serializer.save()
        self.assertEqual(layer.name, 'foo')

        data = {
            'name': 'bar',
            'provider': {
                'name': 'vector',
                'driver': VECTOR_DRIVER_SPATIALITE,
            }
        }
        with self.assertRaises(ValidationError):
            layer = serializer.update(instance=layer, validated_data=data)


class BoundsSerializerTestCase(SimpleTestCase):

    def test_serialize_bounds(self):

        bounds = {
            'low': 1,
            'high': 31,
            'north': 1,
            'west': -1,
            'south': -1,
            'east': 1
        }
        serializer = BoundsSerializer(data=bounds)
        self.assertTrue(serializer.is_valid())

    def test_deserialize_bounds(self):

        bounds = {
            'low': 1,
            'high': 31,
            'north': 1,
            'west': -1,
            'south': -1,
            'east': 1
        }

        serializer = BoundsSerializer(instance=bounds)
        data = serializer.data

        self.assertIsNotNone(serializer)
        self.assertDictEqual(data, bounds)


class ExternalProviderSerializerTestCase(SimpleTestCase):

    def test_init_serializer(self):

        serializer = ExternalProviderSerializer()
        self.assertIsNotNone(serializer)

    def test_serialize(self):

        provider = {
            u'class': 'foo',
        }
        serializer = ExternalProviderSerializer(instance=provider)
        data = serializer.data
        self.assertDictEqual(data, provider)

    def test_deserialize(self):
        data = {
            u'class': 'foo',
        }
        serializer = ExternalProviderSerializer(data=data)
        self.assertTrue(serializer.is_valid())
        provider = serializer.save()
        self.assertTrue(isinstance(provider, dict))
        self.assertDictEqual(data, provider)

    def test_deserialize_invalid(self):
        data = {
            u'foo': 'bar'
        }
        serializer = ExternalProviderSerializer(data=data)
        self.assertFalse(serializer.is_valid())


class MapnikProviderSerializerTestCase(SimpleTestCase):

    def test_init_serializer(self):

        serializer = MapnikProviderSerializer()
        self.assertIsNotNone(serializer)

    def test_serialize(self):

        provider = {
            u'name': 'mapnik',
            u'mapfile': 'foo',
        }
        serializer = MapnikProviderSerializer(instance=provider)
        data = serializer.data
        self.assertDictEqual(data, provider)

    def test_deserialize(self):
        data = {
            u'name': 'mapnik',
            u'mapfile': 'foo',
        }
        serializer = MapnikProviderSerializer(data=data)
        self.assertTrue(serializer.is_valid())
        provider = serializer.save()
        self.assertTrue(isinstance(provider, dict))
        self.assertDictEqual(data, provider)

    def test_deserialize_invalid(self):
        data = {
            u'name': 'mapnik',
        }
        serializer = MapnikProviderSerializer(data=data)
        self.assertFalse(serializer.is_valid())


class ProxyProviderSerializerTestCase(SimpleTestCase):

    def test_init_serializer(self):

        serializer = ProxyProviderSerializer()
        self.assertIsNotNone(serializer)

    def test_serialize(self):

        provider = {
            u'name': 'proxy',
            u'url': 'foo',
        }
        serializer = ProxyProviderSerializer(instance=provider)
        data = serializer.data
        self.assertDictEqual(data, provider)

    def test_deserialize(self):
        data = {
            u'name': 'proxy',
            u'url': 'foo',
        }
        serializer = ProxyProviderSerializer(data=data)
        self.assertTrue(serializer.is_valid())
        provider = serializer.save()
        self.assertTrue(isinstance(provider, dict))
        self.assertDictEqual(data, provider)

    def test_deserialize_invalid(self):
        data = {
            u'name': 'proxy',
        }
        serializer = ProxyProviderSerializer(data=data)
        self.assertFalse(serializer.is_valid())


class VectorProviderSerializerTestCase(SimpleTestCase):

    def test_init_serializer(self):

        serializer = VectorProviderSerializer()
        self.assertIsNotNone(serializer)

    def test_serialize(self):

        provider = {
            'name': 'vector',
            'driver': VECTOR_DRIVER_POSTGRES,
            'parameters': {
                'host': 'foo',
                'dbname': 'bar'
            }
        }
        serializer = VectorProviderSerializer(instance=provider)
        data = serializer.data
        self.assertDictEqual(data, provider)

    def test_deserialize(self):
        data = {
            u'name': 'vector',
            u'driver': VECTOR_DRIVER_POSTGRES,
            u'parameters': {
                u'host': 'foo',
                u'dbname': 'bar'
            },
            u'clipped': True,
            u'precision': 6
        }
        serializer = VectorProviderSerializer(data=data)
        self.assertTrue(serializer.is_valid())
        provider = serializer.save()
        self.assertTrue(isinstance(provider, dict))
        self.assertDictEqual(data, provider)

    def test_deserialize_invalid(self):
        data = {
            'name': 'vector',
            'driver': VECTOR_DRIVER_POSTGRES
        }
        serializer = VectorProviderSerializer(data=data)
        self.assertFalse(serializer.is_valid())
