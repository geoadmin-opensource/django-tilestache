# coding: utf-8
from django.test import SimpleTestCase
from django_tilestache.base import BaseConfiguration


class MockConfig(BaseConfiguration):

    foo = None
    bar = None
    baz = None

    optional_properties = (
        'bar',
        ('baz', 'alias'),
    )

    def is_valid(self):
        return True

    def _to_dict(self):
        dic = {'foo': self.foo}
        return self._merge_properties(dic)


class BaseConfigurationTestCase(SimpleTestCase):

    def test_init_without_options(self):

        config = BaseConfiguration()
        self.assertIsNotNone(config)
        self.assertIsNotNone(config.id)

    def test_init_with_id(self):

        config = BaseConfiguration(id='foo')
        self.assertIsNotNone(config)
        self.assertEqual(config.id, 'foo')

    def test_is_valid_raises(self):

        config = BaseConfiguration()
        with self.assertRaises(NotImplementedError):
            config.is_valid()

    def test_to_dict_raises(self):

        config = BaseConfiguration()
        with self.assertRaises(NotImplementedError):
            config.to_dict()

    def test_merge_optional_properties(self):

        config = MockConfig(**{'foo': 'lalala'})
        dic = config.to_dict()
        self.assertDictEqual(
            {
                'foo': 'lalala',
            },
            dic
        )

    def test_merge_optional_properties_with_values(self):

        config = MockConfig(**{'foo': 'lalala',
                               'bar': 'lololo',
                               'baz': 1234})
        dic = config.to_dict()
        self.assertDictEqual(
            {
                'foo': 'lalala',
                'bar': 'lololo',
                'alias': 1234
            },
            dic
        )
